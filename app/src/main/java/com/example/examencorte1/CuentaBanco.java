package com.example.examencorte1;

public class CuentaBanco {
    private String numCuenta;
    private String nombre;
    private String banco;
    private float saldo;

    public CuentaBanco(String numCuenta, String nombre, String banco, float saldo) {
        this.numCuenta = numCuenta;
        this.nombre = nombre;
        this.banco = banco;
        this.saldo = saldo;
    }

    public float obtenerSaldo() {
        return saldo;
    }

    public void retirarDinero(float cantidad) {
        if (cantidad <= saldo) {
            saldo -= cantidad;
        }
    }

    public void hacerDeposito(float cantidad) {
        saldo += cantidad;
    }

    // Getters y setters para cada atributo
    public String getNumCuenta() {
        return numCuenta;
    }

    public void setNumCuenta(String numCuenta) {
        this.numCuenta = numCuenta;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public float getSaldo() {
        return saldo;
    }

    public void setSaldo(float saldo) {
        this.saldo = saldo;
    }
}