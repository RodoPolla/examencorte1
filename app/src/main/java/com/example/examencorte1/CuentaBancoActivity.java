package com.example.examencorte1;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class CuentaBancoActivity extends AppCompatActivity {

    private EditText txtNumCuenta, txtNombre, txtBanco, txtSaldo, txtCantidad;
    private Spinner spinnerAccion;
    private Button btnRealizarAccion, btnConsultarSaldo, btnLimpiar, btnRegresar;
    private CuentaBanco cuentaBanco;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cuenta_banco);

        txtNumCuenta = findViewById(R.id.txtNumCuenta);
        txtNombre = findViewById(R.id.txtNombre);
        txtBanco = findViewById(R.id.txtBanco);
        txtSaldo = findViewById(R.id.txtSaldo);
        txtCantidad = findViewById(R.id.txtCantidad);
        spinnerAccion = findViewById(R.id.spinnerAccion);
        btnRealizarAccion = findViewById(R.id.btnRealizarAccion);
        btnConsultarSaldo = findViewById(R.id.btnConsultarSaldo);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.acciones, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerAccion.setAdapter(adapter);

        cuentaBanco = new CuentaBanco("123456", "Admin", "BancoX", 1000.0f);

        btnConsultarSaldo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtSaldo.setText(String.valueOf(cuentaBanco.obtenerSaldo()));
            }
        });

        btnRealizarAccion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String accion = spinnerAccion.getSelectedItem().toString();
                if (validarCantidad()) {
                    float cantidad = Float.parseFloat(txtCantidad.getText().toString());
                    if (accion.equals("Depositar")) {
                        cuentaBanco.hacerDeposito(cantidad);
                    } else if (accion.equals("Retirar")) {
                        if (cantidad <= cuentaBanco.obtenerSaldo()) {
                            cuentaBanco.retirarDinero(cantidad);
                        } else {
                            Toast.makeText(CuentaBancoActivity.this, "Saldo insuficiente", Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }
                    txtSaldo.setText(String.valueOf(cuentaBanco.obtenerSaldo()));
                }
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtNumCuenta.setText("");
                txtNombre.setText("");
                txtBanco.setText("");
                txtSaldo.setText("");
                txtCantidad.setText("");
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private boolean validarCantidad() {
        String cantidadStr = txtCantidad.getText().toString().trim();
        if (cantidadStr.isEmpty()) {
            Toast.makeText(this, "Por favor, ingrese una cantidad", Toast.LENGTH_SHORT).show();
            return false;
        }
        try {
            Float.parseFloat(cantidadStr);
        } catch (NumberFormatException e) {
            Toast.makeText(this, "Por favor, ingrese una cantidad válida", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
}